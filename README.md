# Mini Project - Student management

Routing:

# /login

# /admin: layout

# /admin

- feature: /admin/dashboard
- feature: /admin/students

# /auth (authentication)

- login
- sign up / register
- forget password
