import React, { useEffect } from 'react';
import { Counter } from './features/counter/Counter';
import './App.css';
import citiesApi from './api/citiesApi';
import { Route, Routes } from 'react-router-dom';
import LoginPage from './features/auth/pages/LoginPage';
import { AdminLayout } from './components/layout';
import { NotFound } from './components/Common';

function App() {

  useEffect(() => {
    citiesApi.getAll()
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  })

  return (
    <div>
      <Routes>
        {/* Login page */}
        <Route path='/login' element={<LoginPage />} />

        {/* Admin Page */}
        <Route path='/admin' element={<AdminLayout />} />

        {/* Not Found */}
        <Route path='*' element={<NotFound />} />
      </Routes>
    </div>
  );
}

export default App;
