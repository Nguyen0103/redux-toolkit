
import { city } from '../model';
import { ListResponse } from './../model/common';
import axiosClient from "./axiosClient";

const citiesApi = {
    getAll(): Promise<ListResponse<city>> {
        const url = '/cities'
        return axiosClient.get(url, {
            params: {
                _page: 1,
                _limit: 10
            },
        });
    },
};

export default citiesApi;

